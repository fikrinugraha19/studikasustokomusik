import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import pembeli from '@/components/pembeli'
import pembeliform from '@/components/pembeliform'
import alatmusik from '@/components/alatmusik'
import alatmusikform from '@/components/alatmusikform'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: home
    },
    {
      path: '/pembeli',
      name: 'Pembeli',
      component: pembeli
    },
    {
      path: '/pembeli/create',
      name: 'PembeliCreate',
      component: pembeliform
    },
    {
      path: '/pembeli/:id',
      name: 'PembeliEdit',
      component: pembeliform
    },
    {
      path: '/alatmusik',
      name: 'AlatMusik',
      component: alatmusik
    },
    {
      path: '/alatmusik/create',
      name: 'AlatMusikCreate',
      component: alatmusikform
    },
    {
      path: '/alatmusik/:id',
      name: 'AlatMusikEdit',
      component: alatmusikform
    }
  ]
})
