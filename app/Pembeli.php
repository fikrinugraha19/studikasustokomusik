<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Pembeli extends Model
{

  public $table = 'pembeli';

  protected $fillable = ['id','nama_pembeli','alamat'];

}
