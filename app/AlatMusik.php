<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class AlatMusik extends Model
{

  public $table = 'alat_musik';

  protected $fillable = ['nama_barang','harga','stok','merk'];

}
