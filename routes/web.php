<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//pembeli
$router->post('/pembeli','PembeliController@create');
$router->get('/pembeli','PembeliController@read');
$router->post('/pembeli/{id}','PembeliController@update');
$router->delete('/pembeli/{id}','PembeliController@delete');
$router->get('pembeli/{id}','PembeliController@detail');

//Barang
$router->post('/alatmusik','AlatMusikController@create');
$router->get('/alatmusik','AlatMusikController@read');
$router->post('/alatmusik/{id}','AlatMusikController@update');
$router->delete('/alatmusik/{id}','AlatMusikController@delete');
$router->get('alatmusik/{id}','AlatMusikController@detail');
